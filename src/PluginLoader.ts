import NodePath from 'path'
import NodeFs from 'fs'

export type Loadable<T> = (base: T) => T
export interface ImportPlugin<T> {
    default: Loadable<T>
    name?: string
}

export interface PluginLoaderConfig {
    fileNamePart: string,
    path: string,
}

const DefaultPluginLoaderConfig: PluginLoaderConfig = {
    fileNamePart: 'plugin',
    path: '',
}

export interface Plugin<T> {
    name: string
    class: T
}

export class InvalidPluginPathError extends Error {
    constructor(path: string, fileEnd: string) {
        super()
        this.message = `The provided path "${path}" does not end with ${fileEnd}.`
    }
}

export class ModuleNotImportableError extends Error {
    constructor(path: string) {
        super()
        this.message = `The provided path "${path}" is not an importable module.`
    }
}

export class PluginLoader<T> {
    protected config: PluginLoaderConfig
    protected base: T

    protected fileEnd: string

    constructor(base: T, config?: PluginLoaderConfig) {
        this.config = Object.assign({}, DefaultPluginLoaderConfig, config ? config : {})
        this.base = base
        this.fileEnd = '.' + [this.config.fileNamePart, 'js'].join('.')
    }

    protected validatePluginPath(path: string) {
        return path.endsWith(this.fileEnd)
    }

    public loadPlugin(path: string): Plugin<T> {
        if (!this.validatePluginPath(path)) {
            throw new InvalidPluginPathError(path, this.fileEnd)
        }
        return this.loadPluginFromPath(path)
    }

    protected loadImportedPlugin<T>(importedPlugin: ImportPlugin<T>, base: T) {
        return importedPlugin.default(base)
    }

    protected loadPluginFromPath(path: string) {
        let importPlugin = undefined
        try {
            importPlugin = this.require(path);
        } catch (e) {
            throw new ModuleNotImportableError(path)
        }
        const pluginClass = this.loadImportedPlugin(importPlugin, this.base)
        return {
            class: pluginClass,
            name: importPlugin.name ? importPlugin.name : (<ObjectConstructor><unknown>pluginClass)['name']
        }
    }

    protected require(pluginPath: string) {
        return <ImportPlugin<T>>require(pluginPath)
    }

    public load(): Plugin<T>[] {
        const path = this.config.path
        const files = NodeFs.readdirSync(path).filter(file => this.validatePluginPath(file))
        return files.map(file => this.loadPluginFromPath(NodePath.join(path, file)))
    }

}