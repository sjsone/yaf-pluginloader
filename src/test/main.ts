import NodePath from 'path'
import { PluginLoader } from "../PluginLoader"

class Test {
    test() {
        console.log("test")
    }
}

const pluginLoader = new PluginLoader(Test, {
    fileNamePart: 'plugin',
    path: NodePath.resolve(__dirname, './plugins')
})

const plugins = pluginLoader.load()
plugins.forEach(plugin => {
    console.log(plugin)
    const instance = new plugin.class()
    instance.test()
})