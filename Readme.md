# PluginLoader



## Example 

`plugins/bla.plugin.ts`
```typescript
export default (BaseClass: ObjectConstructor) => class Bla extends BaseClass {
    bla() {
        console.log("bla")
    }
}
```

`plugins/blu.plugin.ts`
```typescript
export default (BaseClass: ObjectConstructor) => class Blu extends BaseClass {
    blu() {
        console.log("blu")
    }

    test() {
        console.log("blu test")
    }
}
export const name = 'tescht'
```

`main.ts`
```typescript
import NodePath from 'path'

import { PluginLoader } from "../PluginLoader"


class Test {
    test() {
        console.log("test")
    }
}


const pluginLoader = new PluginLoader(Test, {
    fileNamePart: 'plugin',
    path: NodePath.resolve(__dirname, './plugins')
})


const plugins = pluginLoader.load()
plugins.forEach(plugin => {
    console.log(plugin)
    const instance = new plugin.class()
    instance.test()
})
```